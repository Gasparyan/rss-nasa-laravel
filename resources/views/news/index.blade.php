@extends('layouts.base')

@section('title', 'NASA')

@section('head')
    @parent
@stop

@section('body')
		<div class="table-responsive"> 
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Actions</th>
						<th>Title</th>
						<th><a href="https://www.nasa.gov/">Publication date</a></th>
						<th>Upload date</th>
						<th>Comments</th>
					</tr>
				</thead>
				<tbody>
				@foreach($news as $one)
					<tr>
						<td>{{ $one['id'] }}</td>
						<td><a href="{{ url('edit') }}">Edit</a> or <a href="{{ url('remove') }}">remove</a></td>
						<td><a href="{{ url('article') }}">{{ $one['title'] }}</a></td>
						<td>{{ $one['created_at'] }}</td>
						<td>{{ $one['updated_at'] }}</td>
						<td>5</td>
					</tr>
				@endforeach
				</tbody>
				</table>
		</div>
	</body>
</html>
@stop

<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\RedirectResponse;

class ParsingController extends Controller
{
    private $feedUrl = "https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss";
    
    public function xml()
    {
        if ($this->feedUrl) {
            $xml = @simplexml_load_file($this->feedUrl);
            if (!$xml) {
                echo 'Failed to parse xml file';
            }
            //$json_string = json_encode($xml);
            //$result_array = json_decode($json_string, TRUE);
            //var_dump($json_string);
            //var_dump(count($result_array));
            //var_dump($result_array);
            //exit();
            foreach ($xml->channel->item as $item) {
                $one = new \App\News;
                    $one->title = $item->title;
                    $one->description = $item->description;
                    $item->pubDate = date("Y-m-d H:i:s", strtotime($item->pubDate));
                    $one->created_at = $item->pubDate;
                    $one->save();
                if($one->id = $item->id) {
                    return false;
                }
            }
        }
        return redirect('/');
    }
}

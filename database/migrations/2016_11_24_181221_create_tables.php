<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateTables extends Migration
{
	/**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		//
		//Schema::create('images', function (Blueprint $table) {
			//$table->increments('id');
			//$table->string('pic_path', 200);
			//$table->integer('sort', false)->nullable();
		//});
		
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('login', 32);
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
		});
		
		Schema::create('password_resets', function (Blueprint $table) {
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at');
		});
		
		
		Schema::create('news', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->text('description');
			//$table->integer('news_pic_id')->unsigned();
			//$table->foreign('news_pic_id')->references('id')->on('images');
			//$table->dateTime('created_on');
		});
		

		Schema::create('comments', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->text('description');
			$table->dateTime('created_on');
			//$table->integer('user_id')->unsigned();
			//$table->foreign('user_id')->references('id')->on('users');
		});
	}
		
    /**
     * Reverse the migrations.
     *
     * @return void
     */
	public function down()
	{
		//
		//Schema::drop('images');
		Schema::drop('users');
		Schema::drop('password_resets');
		Schema::drop('news');
		Schema::drop('comments');
	}
}

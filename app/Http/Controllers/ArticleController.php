<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Contracts\View\Factory as View;

class ArticleController extends Controller
{
    /**
     * Our view factory instance
     *
     * @var \Illuminate\Contracts\View\Factory
     */
    protected $view;

    /**
     * Inject our controller dependencies
     *
     * @param \Illuminate\Contracts\View\Factory $view
     */
    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $id=0;
        $article = \App\News::all()->get($id);
        var_dump($article);
        return $this->view->make('article.index', ['article' => $article]);

    }
}